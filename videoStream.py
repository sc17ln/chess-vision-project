import cv2 as cv
import numpy as np
import imutils
import matplotlib.pyplot as plt
from Chessnut import Game
from time import sleep
import chess
import chess.svg
from cairosvg import svg2png
import chess.engine
from gtts import gTTS 
from playsound import playsound
import os
from stockfish import Stockfish
import time

#skill_level: Skill Level option between 0 (weakest level) and 20 (full strength)
STOCKFISH_LEVEL = 10

chessgame = Game()

engine = chess.engine.SimpleEngine.popen_uci("./stockfish-11-mac/Mac/stockfish-11-64")

stockfish = Stockfish("./stockfish-11-mac/Mac/stockfish-11-64")

stockfish.set_skill_level(STOCKFISH_LEVEL)
flag = True
f = 0



#Converts the image to greyscale
def makeGray(frame):
	gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
	return gray

def makeFloat(frame):
	floated = np.float32(frame)
	return floated

#Harris corner detection -> Can be used for a future, more scalable solution
def applyHarrisCorner(frame):
	harrisCornerApplied = cv.cornerHarris(frame,2,3,0.04)
	return harrisCornerApplied

def dilateFrame(frame):
	dilatedFrame = cv.dilate(frame,None)
	return dilatedFrame

def findLines(frame, original):
	#Taken from official documentation: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_houghlines/py_houghlines.html
	minLineLength = 10
	maxLineGap = 10
	lines = cv.HoughLinesP(frame,1,np.pi/180,200,minLineLength,maxLineGap)
	i = 0
	for line in lines:
		for x1,y1,x2,y2 in line:
			cv.line(original,(x1,y1),(x2,y2),(100,0,200),4)

	cv.imwrite('boardCaptures/lines.png', original)


#Detecting outer edges of Chessboard
def edgeDetection(frame, original):
	wideCanny = cv.Canny(frame, 300, 300, apertureSize=3)
	#cv.floodFill(wideCanny, None, (0,0),255)
	#cv.imshow('edgeDetection',cv.bitwise_not(wideCanny))
	return cv.bitwise_not(wideCanny)


#Looping through the contours
def loopContours(frame):
	copy = frame.copy()
	contours, hierarchy = cv.findContours(frame, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
	for i in range(len(contours)):
		area = cv.contourArea(contours[i])
		if (loseContour(contours[i] , area)):
			cv.drawContours(copy, contours, -1, (0,255,0), 2)
	#cv.imshow('WITsH CONTOURS',copy)
	return copy


#Removing unwanted contours, such as noise that has been mistakenly identified as a contour
def loseContour(c, area):
	#https://www.pyimagesearch.com/2015/02/09/removing-contours-image-using-python-opencv/
	#https://www.pyimagesearch.com/2016/02/08/opencv-shape-detection/
	perimetre = cv.arcLength(c, True)
	approx = cv.approxPolyDP(c, 0.02 * perimetre, True)
	(x, y, w, h) = cv.boundingRect(approx)
	aspectRatio = w / float(h)
	i = 0
	if len(approx) == 4:
		if ((aspectRatio >= 0.95)):
			return False

	return True


#Append two images together using a mask
def appendImages(frame, original):
	newFrame = cv.bitwise_and(original, original, mask=frame)
	return newFrame


#Apply a gaussian blur and convert the image into a Binary representation
def blackAndWhite(frame):
	convertSquares = cv.GaussianBlur(frame,(5,5),0)
	_, convertedDrawing = cv.threshold(convertSquares,0,255,cv.THRESH_OTSU)
	kernel = np.ones((5, 5), np.uint8)
	convertedDrawing = cv.morphologyEx(convertedDrawing, cv.MORPH_OPEN, kernel)
	#convertedDrawing = cv.dilate(convertedDrawing,kernel,iterations = 1)
	return convertedDrawing
	#This small excerpt has been taken from: https://tech.bakkenbaeck.com/people/saurabh-b AUTHOR: SAUARBH B
	#This allows us to clearly find the light and dark squares.

#Here we crop to my specific configuration, but this can easily be adapted for the user to manually crop
def cropImage(frame):
	#x1 = 160 x2 = 510
	#y1 = 340 y2 = 380
	x = 95
	x2 = 560
	y = 41
	y2 = 427
	#y1 : y2  ,  x1 : x2
	cropped = frame[y:y2, x:x2]
	return cropped

#Here we can filter the contours to count them in realtime
def filterContours(frame, cropped):
	#Returns bounded rectangles of the dark squares
	contours, hierarchy = cv.findContours(frame, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
	count = 0
	for cnt in contours:
		if cv.contourArea(cnt):
			count = count+1
			rect = cv.minAreaRect(cnt)
			box = cv.boxPoints(rect)
			box = np.int0(box)
			cv.drawContours(cropped,[box],0,(0,0,255),2)
			cv.putText(cropped,"{}".format(count), tuple(box[0]), cv.FONT_HERSHEY_SIMPLEX, 0.6, (0  , 111, 0), 2)
	#cv.imshow('rectangles', cv.cvtColor(cropped,cv.COLOR_GRAY2RGB))
	cv.imwrite('boardCaptures/rectangles.png',cropped)
	return cropped
	#Must be 32.


#After detecting outer chessboard, further cropping in order to draw the grid
#Crop to fit largest contour
#This very helpful function has been written (Mostly) by: Andriy Makukha https://stackoverflow.com/users/5407270/andriy-makukha
def adjustFrameSize(frame, original):
	contours, hierarchy = cv.findContours(frame, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
	mx = (0,0,0,0)      # biggest bounding box so far
	mx_area = 0
	for cont in contours:
		x,y,w,h = cv.boundingRect(cont)
		area = w*h
		if area > mx_area:
			mx = x,y,w,h
			mx_area = area
	x,y,w,h = mx
	roi=original[y:y+h,x:x+w]
	#cv.imshow('Image_crop.jpg', roi)
	return roi



def chessCorners(cropped, original):
	ogCopy = original
	copy = cropped

	#IMAGE PRE TRANSFORMATION INTO GRID LIKE SHAPE
	copyToKeep = copy.copy()


	#TRANSFORMS IMAGE INTO 300x300 GRID WITH LINES AND COORDINATES
	transformed = createSquareCoord(0, copyToKeep, False)


	#THIS HOLDS FRAME TO ANALYSE
	frameToAnalyse = plotSquares(transformed, transformed)
	#cv.imshow('Frame', frameToAnalyse)


	#CALCULATES CORNERS OF SQUARES USING ASPECT RATIO CALCULATION
	squares= regionsOfInterest(frameToAnalyse, frameToAnalyse)
	

	#RETURNS ALL THE COORDINATES OF PIECES ALONG WITH LABELED ARRAY (E.G wP on A5)
	# boarReferences = (Coords of square, piece on square, chessboardSquareReference)

	# GET NAME OF PIECE ON BOARD = boardReferences([indexOfSquare] [0] [1])

	# GET REGION OF INTERES (AKA, COORDINATES OF SQUARE REGION ON BOARD = boardReferences([indexOfSquare] [0] [0])

	# GET NAME OF CHESSSBOARD SQUARE = boardReferences([indexOfSquare] [1] )
	boardReferences = buildBoard(squares)


	#GETS INITIAL COLOUR VALUES, AFTER PIECES HAVE BEEN PLACED
	#colourValuesOfSquares = getColourValues(boardReferences)



	return boardReferences, frameToAnalyse

	# #THIS SECTION OF CODE IS FOR SCALABLE BOARD RECOGNITION -> USES EDGE RECOGNITION -> CONTOUR SHADING AND RECTANGLE RECOGNTION TO FIND DARK SQUARES
	# #Is better for low lighting settings or weird angles
	# #IT HAS BEEN COMMENTED OUT ON PURPOSE!
	# #A DEMONSTRATION OF ITS FUNCTIONALITY HAS BEEN SHOWN IN THE BOARD CAPTURES FOLDER, CONTAINING IMAGES OF USES OF THIS OTHER TECHNIQUE
	



	# #Convert Frame to Black and White
	# blackWhite = blackAndWhite(cropped)
	# blackWhiteRevert = cv.bitwise_not(blackWhite)
	# adjusted = adjustFrameSize(blackWhiteRevert, copy)
	# edges = edgeDetection(cv.bitwise_not(adjusted), cv.bitwise_not(adjusted))
	# cv.imshow('test', cv.bitwise_not(edges))
	# lightSquares = loopContours(edges)
	# #filtered = filterContours(lightSquares, lightSquares.copy())	


	# #Adjust the contours (Dark squares on chess board)
	# contoursAdjusted = loopContours(appended)

	# #Highlight edges through canny methods
	# edges = edgeDetection(contoursAdjusted,contoursAdjusted)

	# #RETURNS LIGHT SQUARES
	# filtered = filterContours(edges, adjusted)

	# #Connect the lines of the grid
	# lines = findLines(cv.bitwise_not(blackWhite), cv.bitwise_not(blackWhite))
	
	# #This part has closely followed this official documentation and tutorial: 
	# #https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_calib3d/py_calibration/py_calibration.html

	# #This is copied, as there is no other way to achieve such, you MUST utilize the preset criteria that the library provides.
	# #The 3D array also MUST be populated in this specific way
	# criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
	# #Prepares an array to hold the 3D coordinates
	# objectPrep = np.zeros((5*5,3), np.float32)
	# objectPrep[:,:2] = np.mgrid[0:5,0:5].T.reshape(-1,2)

	# #Where we will store all the resulting coordinates of the corners
	# #2D References to the points
	# images = []
	# #3D References to the points
	# objects = []

	# #Find corners and store them as coordinates, with subsquent type issuing
	# resized = cropImage(copy)
	# temp = resized.copy()
	# ret, resized =  cv.threshold(resized,64,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	
	# kernel = np.ones((5, 5), np.uint8)
	# eroded = cv.morphologyEx(resized, cv.MORPH_OPEN, kernel)
	# #cv.imshow('after', eroded)
	# laterCopy = eroded.copy()


	# #resized = cv.bitwise_not(resized, eroded)
	# #resized = cv.bitwise_not(resized)
	# #cv.imshow('to be analysed', eroded)
	# coordinates, corners = cv.findChessboardCorners(eroded, (5,5), None)
	# #cv.imshow('copy', resized)
	# out = cv.drawChessboardCorners(eroded, (5,5), corners, coordinates)
	# #We can only continue if it actually finds the corners
	# if coordinates == True:

	# 	#Initialize the array with the required dimensions
	# 	objects.append(objectPrep)
		
	# 	#Find the extremeties using threshold criteria, pre-set by OpenCV
	# 	extremeCorners = cv.cornerSubPix(eroded,corners,(11,11),(-1,-1),criteria)
		
	# 	#Append the 2d coordinates of the corners
	# 	images.append(extremeCorners)
		
	# 	#Draw the new image, now with the corners attached
	# 	newDrawing = cv.drawChessboardCorners(eroded, (5,5), extremeCorners, coordinates)
	# 	cv.imwrite('boardCaptures/corners.png', newDrawing)
	# 	createSquareCoord(extremeCorners, laterCopy, True)
	# 	#cv.imshow('corner', newDrawing)

	



#Takes a snapshot of the current image provided
def takeSnapshot(frame):
	
	image = cv.imwrite('boardCaptures/currentBoard.png',frame)
	image = cv.imread('boardCaptures/currentBoard.png')
	
	return image

#Initial corner detection helper
def detectCorners(frame):


	screenshot =  takeSnapshot(frame)

	#gray = makeGray(screenshot)

	#cropped = cropImage(gray)

	new = chessCorners(screenshot, screenshot)

	return new


#Generates new frame into smaller grid representation
def createSquareCoord(intialCoord, frame, flag):
	#cv.imshow('frame', frame)

	#MODIFY THIS TO CROP INTO CHESS GRID AND CHANGE THE PERSPECTIVE OF FRAME
	coordinateSet = intialCoord;
	cornerPoints = np.float32( [ [66,27], [401,38], [50,380], [403,379] ])
	newPoints = np.float32( [ [0,0], [300,0], [0,300], [300,300] ] )
	newPerspective = cv.getPerspectiveTransform(cornerPoints,newPoints)
	newFrame = cv.warpPerspective(frame,newPerspective,(300,300))
	if flag:
		updateCorners(newFrame)

	return newFrame

#Finds the regions of interest aka the chessboard squares
def regionsOfInterest(frame, original):
	#Resourced by: https://mc.ai/sudoku-solver-using-opencv-and-dl%E2%80%8A-%E2%80%8Apart-1/

	frameTemp = frame
	squares = [] 
	side = frameTemp.shape[:1] 
	side = side[0] / 8
	count = 0
	for j in range(8):
		for i in range(8):
			count = count + 1
			p1 = (i * side, j * side) #Top left corner of a box 
			p2 = ((i + 1) * side, (j + 1) * side) #Bottom right corner 
			squares.append((p1, p2)) 
	return squares


#Builds the internal representation of the board
def buildBoard(squares):
	#This assigns the squares to the initial chess board
	#It then joins the square locations to the pieces assigned to it, this will be used to keep track of the game
	copy = squares
	whitePieces = ['wP', 'wB', 'wN', 'wQ', 'wK', 'wR']
	blackPieces = ['bP', 'bB', 'bN', 'bQ', 'bK', 'bR']

	positionList=['bR', 'bN', 'bB', 'bQ', 'bK', 'bB', 'bN', 'bR', 'bP','bP','bP','bP','bP','bP','bP','bP',' ',' ',\
	' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',\
	'wP','wP','wP','wP','wP','wP','wP','wP','wR', 'wN', 'wB','wQ','wK', 'wB','wN','wR']

	squareName = ['A8', 'B8', 'C8', 'D8', 'E8','F8', 'G8', 'H8','A7', 'B7', 'C7', 'D7', 'E7','F7', 'G7','H7',\
	'A6', 'B6', 'C6', 'D6', 'E6','F6','G6', 'H6','A5', 'B5', 'C5', 'D5', 'E5','F5', 'G5', 'H5',\
	'A4', 'B4', 'C4', 'D4', 'E4','F4', 'G4', 'H4' , 'A3', 'B3', 'C3', 'D3', 'E3','F3', 'G3', 'H3', \
	'A2', 'B2', 'C2', 'D2', 'E2','F2','G2', 'H2', \
	'A1', 'B1', 'C1', 'D1', 'E1','F1', 'G1', 'H1']
	correspondingPiece = list(zip(copy, positionList))
	squareRef = list(zip(correspondingPiece, squareName))

	
	# squareRef = (Coords of square, piece on square, chessboardSquareReference)

	# GET NAME OF PIECE ON BOARD = squareRef([indexOfSquare] [0] [1])

	# GET REGION OF INTERES (AKA, COORDINATES OF SQUARE REGION ON BOARD = squareRef([indexOfSquare] [0] [0])

	# GET NAME OF CHESSSBOARD SQUARE = squareRef([indexOfSquare] [1] )
	
	
	


	return squareRef


#Finds the colour values
def getColourValues(squares, frame):

	#Since the square array will ALWAYS be 64 long, we can attribute colour values to equivalent index into values list
	middleOfSquare = []
	values = []


	#LAST PARAMETER 0 or 1
	#FIRST PARAMETER -> index of square
	i = 0
	while i < 64:
		topLeft = squares[i][0][0][0]
		bottomRight = squares[i][0][0][1]

		#We must take the central pixels, in order to most accurately determine colour changes
		middleCoordinatesX = (topLeft[0] + bottomRight[0])/2
		middleCoordinatesY = (topLeft[1] + bottomRight[1] ) /2
		newCoordinate = (middleCoordinatesX, middleCoordinatesY)

		middleOfSquare.append(newCoordinate)
		i = i + 1


	g = 0
	while g < 64:
		x = middleOfSquare[g][0]
		y = middleOfSquare[g][1]
		x = int(x)
		y = int(y)

		colourValue = frame[y, x]
		int1 = int(colourValue[0])
		int2 = int(colourValue[1])
		int3 = int(colourValue[2])
		average = (int1 + int2 + int3)/3

		values.append(average)
		g = g+1

	return values


#Plot the lines on the newly cropped image
#New Frame already fits 300x300
def plotSquares(frame, original):


	copy = frame



	#THIS IS FOR SCALABLE VERSION -> THE NEW FRAME IS MODIFIED INTO 300x300
	#THIS WOULD AUTOMATICALLY PLOT POINTS OF EACH CORNER OF EACH SQUARE ON THE BOARD
	
	# xvalues = np.array([0, 37, 74, 111, 148,185,222,259,299]);
	# yvalues = np.array([0, 37, 74, 111, 148,185,222,259,299]);
	# xcoords, ycoords = np.meshgrid(xvalues, yvalues)
	# newArray = []
	# for x, y in zip(xcoords, ycoords):
	# 	for xx, yy in zip(x, y):
	# 		newArray.append([xx,yy])


	# print(newArray)
	# for point in newArray:
	# 	print(point)
	# 	print('end')
	# 	#copy[point] = 255

	# flipped = np.flip(newArray, 0)
	

	lightSquares = copy.copy()

	#cv2.line(img, (start_x, start_y), (end_x, end_y), (255, 0, 0), 1, 1)
	#Vertical Lines
	cv.line(lightSquares, (0, 0), (300, 0), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (0, 37), (300, 37), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (0, 74), (300, 74), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (0, 111), (300, 111), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (0, 148), (300, 148), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (0, 185), (300, 185), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (0, 222), (300, 222), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (0, 259), (300, 259), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (0, 300), (300, 300), (0, 111, 0), 3, 1)

	#Horizontal Lines
	cv.line(lightSquares, (0, 0), (0, 300), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (37, 0), (37, 300), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (74, 0), (74, 300), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (111, 0), (111, 300), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (148, 0), (148, 300), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (185, 0), (185, 300), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (222, 0), (222, 300), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (259, 0), (259, 300), (0, 111, 0), 3, 1)
	cv.line(lightSquares, (300, 0), (300, 300), (0, 111, 0), 3, 1)

	cv.imwrite('boardCaptures/snapShotInitial.png', lightSquares)
	return lightSquares

	#darkSquares = cv.bitwise_not(copy.copy())

	#Light Squares
	#cv.imshow('one set of suqraes',filterContours(lightSquares, lightSquares))

	#Dark Squares
	#cv.imshow('one set of suqraes',filterContours(darkSquares, darkSquares))






def updateCorners(frame):
	#This is the same as the initial corner detection, but now we can guarantee that the full board is in perfect view
	#We can now get all corners.

	copy = frame.copy()

	#This is copied, as there is no other way to achieve such, you MUST utilize the preset criteria that the library provides.
	#The 3D array also MUST be populated in this specific way
	criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
	#Prepares an array to hold the 3D coordinates
	objectPrep = np.zeros((7*7,3), np.float32)
	objectPrep[:,:2] = np.mgrid[0:7,0:7].T.reshape(-1,2)
	#Where we will store all the resulting coordinates of the corners
	#2D References to the points
	images = []
	#3D References to the points
	objects = []

	coordinates, corners = cv.findChessboardCorners(frame, (7,7), None)
	#cv.imshow('copy', resized)
	out = cv.drawChessboardCorners(frame, (7,7), corners, coordinates)
	#We can only continue if it actually finds the corners
	if coordinates == True:

		#Initialize the array with the required dimensions
		objects.append(objectPrep)
		
		#Find the extremeties using threshold criteria, pre-set by OpenCV
		extremeCorners = cv.cornerSubPix(frame,corners,(11,11),(-1,-1),criteria)
		
		#Append the 2d coordinates of the corners
		images.append(extremeCorners)
		
		#Draw the new image, now with the corners attached
		newDrawing = cv.drawChessboardCorners(frame, (7,7), extremeCorners, coordinates)
		cv.imwrite('boardCaptures/updatedCorners.png', newDrawing)
		#cv.imshow('updatedCorners', newDrawing)
		#plotSquares(copy, copy)
	

#Find the biggest change in colour from one move to the next
def findBiggestChange(arg1, arg2):
	i = 0
	biggestDifference = 0
	biggestIndex = 0
	secondBiggest = 0
	secondBiggestIndex = 0
	currentDifference = 0
	while i < 64:
		arg1Int = int(arg1[i])
		arg2Int = int(arg2[i])
		currentDifference = abs(arg2Int - arg1Int)
		if (currentDifference > biggestDifference):
			secondBiggest = biggestDifference
			secondBiggestIndex = biggestIndex
			biggestDifference = currentDifference
			biggestIndex = i

		elif (currentDifference > secondBiggest):
			secondBiggest = currentDifference
			secondBiggestIndex = i
		i = i + 1

	#print(biggestIndex, secondBiggestIndex)
	#print(biggestDifference, secondBiggest)
	return biggestIndex, secondBiggestIndex

#Saves the still frames to disk
def stillFrame(frame):
	global f
	f = f+1
	cv.imwrite('boardCaptures/useFeed'+str(f)+'.png', frame)
	still = cv.imread('boardCaptures/useFeed'+str(f)+'.pgn')
	path = 'boardCaptures/useFeed'+str(f)+'.png'
	return path

def initBoardSetup():
	

	#Let us name the pieces as StockFish API needs
	#White Pieces = Capital letters
	#Black Pieces = lowercase letters
	positionList=['r', 'n', 'b', 'q', 'k', 'b', 'n', 'r', 'p','p','p','p','p','p','p','p',' ',' ',\
	' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',\
	'P','P','P','P','P','P','P','P','R', 'N', 'B','Q','K', 'B','N','R']
	
	squareName = ['a8', 'b8', 'c8', 'd8', 'e8','f8', 'g8', 'h8','a7', 'b7', 'c7', 'd7', 'e7','f7', 'g7','h7',\
	'a6', 'b6', 'c6', 'd6', 'e6','f6','g6', 'h6','a5', 'b5', 'c5', 'd5', 'e5','f5', 'g5', 'h5',\
	'a4', 'b4', 'c4', 'd4', 'e4','f4', 'g4', 'h4' , 'a3', 'b3', 'c3', 'd3', 'e3','f3', 'g3', 'h3', \
	'a2', 'b2', 'c2', 'd2', 'e2','f2','g2', 'h2', \
	'a1', 'b1', 'c1', 'd1', 'e1','f1', 'g1', 'h1']

	positions = list(zip(positionList, squareName))
	
	return positions, positionList


#Gets the next move
def getNextMove(previousColourValues, positions):
	print(chessgame.get_moves())
	
	print('Begining next move, please move the piece now')
	
	colourValues = previousColourValues
	
	setup2 = startStream(True)


	colourValues2 = getColourValues(setup2[0], setup2[1])


	change = findBiggestChange(colourValues, colourValues2)



	#This contains the actual Square references, of what has changed e.g. a2 and a3
	squareOne = positions[change[0]]
	squareTwo = positions[change[1]]
	squareOne = squareOne[1]
	squareTwo = squareTwo[1]

	print(squareOne)
	print(squareTwo)

	#print(convertToFEN)

	#chessgame.apply_move(squareTwo + squareOne)
	applyMove(squareOne, squareTwo)
	fenNotation = str(chessgame)
	board = chess.Board(fenNotation)
	print(board)



	picture = chess.svg.board(board=board)
	svg2png(bytestring=picture,write_to='boardCaptures/digital.png')
	

	#getNextMove(colourValues2)
	return fenNotation, colourValues2, chessgame


#gets the stockfish engine evaluation
def getEvaluation(fen):
	language = "en"

	currentBoard = chess.Board(fen)
	print(fen)
	info = engine.analyse(currentBoard, chess.engine.Limit(depth=20))

	text = info["score"]
	textString = str(text)
	intCon = int(textString)/100.0
	textAgain = str(intCon)
	speech = gTTS(text = textAgain, lang = language, slow = True)
	speech.save("boardCaptures/evaluation.mp3")
	playsound("boardCaptures/evaluation.mp3")




#Returns best computer move
def getComputerMove(fen):
	language = "en"
	position = stockfish.set_fen_position(fen)
	move = stockfish.get_best_move()
	applyCompMove(move)

	text = str(move)
	speech = gTTS(text = text, lang = language, slow = True)
	
	speech.save("boardCaptures/computerMove.mp3")
	
	playsound("boardCaptures/computerMove.mp3")

	fenNotation = str(chessgame)
	board = chess.Board(fenNotation)
	picture = chess.svg.board(board=board)
	svg2png(bytestring=picture,write_to='boardCaptures/digital.png')
	fenNotation = str(chessgame)
	board = chess.Board(fenNotation)
	print(board)
	return fenNotation, chessgame


#Applies the prescribed computer move
def applyCompMove(move):
	chessgame.apply_move(move)
	fen = str(chessgame)
	board = chess.Board(fen)
	picture = chess.svg.board(board=board)
	svg2png(bytestring=picture,write_to='boardCaptures/digital.png')
	return fen, chessgame
	pass


#Allow the user to make the computer move on behalf of the computer
def reAdjustColour():
	print('Please make the move on behalf of the AI.')
	resetup = startStream(True)
	newColours = getColourValues(resetup[0], resetup[1])
	return newColours
	pass


#Announces checkmate should it be on the board. This is called after every single move
def announceCheckMate(fen):
	currentBoard = chess.Board(fen)
	if (currentBoard.is_checkmate()):
		language = "en"
		textAgain = 'Checkmate!'
		speech = gTTS(text = textAgain, lang = language, slow = True)
		speech.save("boardCaptures/checkmate.mp3")
		playsound("boardCaptures/checkmate.mp3")


#Applying a human move by using the square references
def applyMove(squareOne,squareTwo):
	#We need to try and apply the move twice, to ensure it is being done correctly
	#The try except block has NOT been written by me: https://stackoverflow.com/questions/4606919/in-python-try-until-no-error
	retry = 2
	for x in range(0, retry):
		str_error = None
		if x == 1:
			try:
				chessgame.apply_move(squareOne + squareTwo)
				str_error = None
			
			except Exception as str_error:
				pass

		try:
			chessgame.apply_move(squareTwo + squareOne)
			str_error = None
		except Exception as str_error:
			pass
		else:
			break


def startStream(flag):
	print('init frame')
	#Modify this number if board is not recognized within X iterations
	numberOfIterations = 3

	#0 -> First WebCam
	#1 -> External Camera
	#URL -> Use IP camera
	url = 'http://192.168.0.11/live'


	if (cv.VideoCapture(url).isOpened()):
		videoStream = cv.VideoCapture(url)

	while flag == True:
		print(numberOfIterations)
		ret, frame = videoStream.read()
		if ret == True:
			temp = stillFrame(frame)
			still = cv.imread(temp)
			#cv.imshow('Normal Game',still)
		

			if (numberOfIterations > 0):
				detectCorners(still)
				numberOfIterations = numberOfIterations - 1

			if (numberOfIterations == 0):

				#HOLDS ALL THE INFORMATION OF INITIAL GAME SETUP
				initialBoardSetup = detectCorners(still)
				intialPiecePlacement = initialBoardSetup[0]
				initialFrame = initialBoardSetup[1]

				cv.imwrite('boardCaptures/firstFrame.png', initialFrame)
				cv.destroyAllWindows()

				return intialPiecePlacement, initialFrame




		quit = cv.waitKey(1)
		if quit == 'q':
			break

def initGame():
	#Initial Call to recognize Board, Pieces and subsequent colour values
	setup = startStream(True)
	#Sets up initial starting configuration
	positions = initBoardSetup()[0]
	convertToFEN = initBoardSetup()[1]
	#Begins actual chess game class instance of chessnut and StockFish
	chessgame = Game()
	fen = str(chessgame)
	board = chess.Board(fen)
	picture = chess.svg.board(board=board)
	svg2png(bytestring=picture,write_to='boardCaptures/digital.png')
	return setup[0], setup[1], chessgame, positions






