#Lock needed in order to have two running windows of the video capture
#This is because python tries to access the same video but doesnt hold the lock for the first process, and so only one window was appearing
#Excerpt taken verbatim from: https://stackoverflow.com/questions/35090521/python-script-hangs-on-importing-module-with-multiprocessing-code
import imp
if imp.lock_held():
	imp.release_lock()
	import videoStream as vs
	imp.acquire_lock() 
#End of copied excerpt


#USED FOR DEBUGGING PURPOSES ONLY
def recognizeBoard():
	print("recognizeBoard")

def promotePawn():
	print("Promote Pawn")

def recognizePieces():
	print("Recognize")

def startGame():
	pass

def resign():
	print("Resign")

def toggleBlunderNotification():
	print("Toggle Blunder")

def announceEvaluation():
	print("Announce Evaluation")

def resignGame():
	print("Resign game")

def confirmMove():
	print("Confirm Move")

def takebackMove():
	print("takebackMove")


def saveGame():
	print("saveGame")