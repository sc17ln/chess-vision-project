from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.popup import Popup
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
import chessFunctions as cF
from kivy.properties import ObjectProperty
from kivy.clock import Clock
from Chessnut import Game
import os
from stockfish import Stockfish
import imp
import time
import chess
import videoStream as vS
if imp.lock_held():
	imp.release_lock()
	import videoStream as vS
	imp.acquire_lock() 


#CHANGE FROM 1-20 DEPENDING ON DIFFICULTY WANTED
STOCKFISH_LEVEL = 20

engine = chess.engine.SimpleEngine.popen_uci("./stockfish-11-mac/Mac/stockfish-11-64")

stockfish = Stockfish("./stockfish-11-mac/Mac/stockfish-11-64")

stockfish.set_skill_level(STOCKFISH_LEVEL)





setup = ''
currentColours = None
currentMove = None
chessgame = None
positions = None

class InitialScreen(Screen):
	def start(self):
		global setup
		global currentColours
		global chessgame
		global positions
		setup = vS.initGame()
		positions = setup[3]
		print(positions)
		chessgame = setup[2]
		currentColours = vS.getColourValues(setup[0], setup[1])
		pass


class GameScreen(Screen):
	path = './boardCaptures/digital.png'
	def showPopup(self):
		popup = self.MyPopup()
		popup.open()

	def showCompPopup(self):
		popup = self.MySecondPopup()
		popup.open()

	class MyPopup(Popup):
		pass

	class MySecondPopup(Popup):
		pass


	def reloadImage(self):
		path = './boardCaptures/digital.png'
		self.ids.game.source = path
		Clock.schedule_interval(lambda dt: self.ids.game.reload(), 2)
		return self.ids.game.source
	
	def saveButton(self):

		cF.saveGame()
	
	def announceEval(self):
		global engine
		global stockfish
		vS.getEvaluation(str(chessgame))
		cF.announceEvaluation()

	def applyComputerMove(self):
		global currentColours
		currentColours = vS.reAdjustColour()

	def castleShort(self):
		global currentColours
		global chessgame
		global currentColours
		global currentMove
		#Can apply to white or black because applyMove tries e1g1 and the opposite
		castleShort = vS.applyCompMove('e1g1')
		chessgame = castleShort[1]
		currentColours = vS.reAdjustColour()
		currentMove = (castleShort[0], currentColours, chessgame)

	def castleLong(self):
		global currentColours
		global chessgame
		global currentColours
		global currentMove
		#Can apply to white or black because applyMove tries e1c1 and the opposite
		castleShort = vS.applyCompMove('e1c1')
		chessgame = castleShort[1]
		currentColours = vS.reAdjustColour()
		currentMove = (castleShort[0], currentColours, chessgame)		


	def computerMove(self):
		global chessgame
		global currentMove
		global currentColours
		computerMove = vS.getComputerMove(currentMove[0])
		chessgame = computerMove[1]
		vS.announceCheckMate(str(chessgame))
		cF.toggleBlunderNotification()

	def confirm(self):
		global currentMove
		global chessgame
		global currentColours
		currentMove = vS.getNextMove(currentColours, positions)
		currentColours = currentMove[1]
		chessgame = currentMove[2]
		vS.announceCheckMate(str(chessgame))
		cF.confirmMove()

	def castle(self):
		cF.castle()

	def resign(self):
		pass


class WindowManager(ScreenManager):

	pass

kv = Builder.load_file("builder.kv")

class ChessBuddy(App):
	gameRef = GameScreen()
	def build(self):
		return kv

if __name__ == '__main__':
	ChessBuddy().run()


#LOGO MADE ON https://logomakr.com/
